package com.ascendcorp.exam.util;

public enum ResponseBankType {

	approved,
	invalid_data,
	transaction_error,
	unknown
}
