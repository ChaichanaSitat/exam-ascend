package com.ascendcorp.exam.util;

public enum Response {

	APPROVED(200 ,"approved"),
	INVALID_DATA(400 , "General Invalid Data"),
	TRANSACTION_ERROR(500 , "General Transaction Error"),
	UNKNOWN(501 ,"General Invalid Data"),
	
	NPE(500 ,"General Invalid Data"),
	TIME_OUT(503 , "Error timeout"),
	INTERNAL_APLLICATION_ERROR(504 , "Internal Application Error");
	
	private final Integer code;
    private final String desc;
	
	Response(int code, String desc) {
		// TODO Auto-generated constructor stub
		this.code = code;
		this.desc = desc;
		
	}
	
	public String getDesc() {
        return desc;
    }
	public Integer getCode() {
		return code;
	}
	
}
