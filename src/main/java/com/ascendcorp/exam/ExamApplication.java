package com.ascendcorp.exam;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.ascendcorp.exam.config.MessageConfig;

@SpringBootApplication
public class ExamApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExamApplication.class, args);
	}
	
	@Autowired MessageConfig msg;
	
	@PostConstruct
    private void postConstruct() {
       System.err.println(msg.getMessageSource("error500_code"));
       System.err.println(msg.getMessageSource("error500_desc"));
    }
	
	
}
