package com.ascendcorp.exam.config;

import java.text.MessageFormat;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;

@Configuration
public class MessageConfig {

    @Autowired
    private MessageSource messageSource;
    

    public String getMessageSource(String msg){
        return  messageSource.getMessage(msg,null,new Locale("en"));
    }

    public String getMessageSource(String msg,String errorCode){
        String messageDisplay = messageSource.getMessage(msg,null,new Locale("th"));
        return  String.format("%s %s",messageDisplay,errorCode);
    }

    public String getMessageSourceFmt(String messageCode, Object[] replaceValue){

        String errorMessage = messageSource.getMessage(messageCode, null, new Locale("th"));
        MessageFormat fmt = new MessageFormat(errorMessage);
        return fmt.format(replaceValue);

    }
}
