package com.ascendcorp.exam.model;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class InquiryRequest {

	@NonNull
	String transactionId;
    Date tranDateTime;
    String channel;
    String locationCode;
    String bankCode;
    String bankNumber;
    double amount;
    String reference1;
    String reference2;
    String firstName;
    String lastName;
}
