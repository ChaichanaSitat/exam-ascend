package com.ascendcorp.exam.service.impl;

import com.ascendcorp.exam.model.InquiryServiceResultDTO;
import com.ascendcorp.exam.model.TransferResponse;
import com.ascendcorp.exam.service.ManageResponse;
import com.ascendcorp.exam.util.Response;

public class Approvedimpl implements ManageResponse{

	@Override
	public InquiryServiceResultDTO process(TransferResponse response) {
		// TODO Auto-generated method stub
		return InquiryServiceResultDTO.builder()
				.ref_no1(response.getReferenceCode1())
				.ref_no2(response.getReferenceCode2())
				.amount(response.getBalance())
				.tranID(response.getBankTransactionID())
				.reasonCode(Response.APPROVED.getCode()+"")
				.reasonDesc(response.getDescription())
				.accountName(null).build();
	}



}
