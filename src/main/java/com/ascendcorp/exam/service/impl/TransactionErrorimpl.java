package com.ascendcorp.exam.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.ascendcorp.exam.model.InquiryServiceResultDTO;
import com.ascendcorp.exam.model.TransferResponse;
import com.ascendcorp.exam.service.ManageResponse;
import com.ascendcorp.exam.util.Response;

public class TransactionErrorimpl implements ManageResponse{
	
	 final static Logger log = Logger.getLogger(TransactionErrorimpl.class);

	@Override
	public InquiryServiceResultDTO process(TransferResponse response) {
		// TODO Auto-generated method stub
		InquiryServiceResultDTO respDTO = new InquiryServiceResultDTO();
		respDTO.setRef_no1(response.getReferenceCode1());
        respDTO.setRef_no2(response.getReferenceCode2());
        respDTO.setAmount(response.getBalance());
        respDTO.setTranID(response.getBankTransactionID());
		
        // bank response code = transaction_error
        String replyDesc = response.getDescription();
        if(StringUtils.isEmpty(replyDesc)) {
        	respDTO.setReasonCode(Response.TRANSACTION_ERROR.getCode()+"");
            respDTO.setReasonDesc(Response.TRANSACTION_ERROR.getDesc());
            return respDTO;
        }
        

            String respDesc[] = replyDesc.split(":");
            if(respDesc != null && respDesc.length >= 2)
            {
                log.info("Case Inquiry Error Code Format Now Will Get From [0] and [1] first");
                String subIdx1 = respDesc[0];
                String subIdx2 = respDesc[1];
                log.info("index[0] : "+subIdx1 + " index[1] is >> "+subIdx2);
                if("98".equalsIgnoreCase(subIdx1))
                {
                    // bank code 98
                    respDTO.setReasonCode(subIdx1);
                    respDTO.setReasonDesc(subIdx2);
                }else{
                    log.info("case error is not 98 code");
                    if(respDesc.length >= 3){
                        // bank description full format
                        String subIdx3 = respDesc[2];
                        log.info("index[0] : "+subIdx3);
                        respDTO.setReasonCode(subIdx2);
                        respDTO.setReasonDesc(subIdx3);
                    }else{
                        // bank description short format
                        respDTO.setReasonCode(subIdx1);
                        respDTO.setReasonDesc(subIdx2);
                    }
                }
            }else{
                // bank description incorrect format
                respDTO.setReasonCode(Response.TRANSACTION_ERROR.getCode()+"");
                respDTO.setReasonDesc(Response.TRANSACTION_ERROR.getDesc());
            }

		return respDTO;
	}

}
