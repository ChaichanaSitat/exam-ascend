package com.ascendcorp.exam.service.impl;

import org.apache.commons.lang3.StringUtils;

import com.ascendcorp.exam.model.InquiryServiceResultDTO;
import com.ascendcorp.exam.model.TransferResponse;
import com.ascendcorp.exam.service.ManageResponse;
import com.ascendcorp.exam.util.Response;

public class InvalidDataimpl implements ManageResponse {

	@Override
	public InquiryServiceResultDTO process(TransferResponse response) {
		// TODO Auto-generated method stub
		InquiryServiceResultDTO respDTO = new InquiryServiceResultDTO();
		respDTO.setRef_no1(response.getReferenceCode1());
        respDTO.setRef_no2(response.getReferenceCode2());
        respDTO.setAmount(response.getBalance());
        respDTO.setTranID(response.getBankTransactionID());

		// bank response code = invalid_data
		String replyDesc = response.getDescription();

		if (StringUtils.isEmpty(replyDesc)) {
			respDTO.setReasonCode(Response.INVALID_DATA.getCode()+"");
			respDTO.setReasonDesc(Response.INVALID_DATA.getDesc());
			return respDTO;
		}

		String respDesc[] = replyDesc.split(":");
		if (respDesc != null && respDesc.length >= 3) {
			// bank description full format
			respDTO.setReasonCode(respDesc[1]);
			respDTO.setReasonDesc(respDesc[2]);
		} else {
			// bank description short format
			respDTO.setReasonCode(Response.INVALID_DATA.getCode()+"");
			respDTO.setReasonDesc(Response.INVALID_DATA.getDesc());
		}

		return respDTO;
	}

}
