package com.ascendcorp.exam.service;

import com.ascendcorp.exam.model.InquiryServiceResultDTO;
import com.ascendcorp.exam.model.TransferResponse;

public interface ManageResponse {

	public InquiryServiceResultDTO process(TransferResponse response);
}
