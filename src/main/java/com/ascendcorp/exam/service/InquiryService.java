package com.ascendcorp.exam.service;

import java.util.Date;
import java.util.HashMap;


import javax.xml.ws.WebServiceException;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ascendcorp.exam.config.MessageConfig;
import com.ascendcorp.exam.model.InquiryServiceResultDTO;
import com.ascendcorp.exam.model.TransferResponse;
import com.ascendcorp.exam.proxy.BankProxyGateway;
import com.ascendcorp.exam.service.impl.Approvedimpl;
import com.ascendcorp.exam.service.impl.InvalidDataimpl;
import com.ascendcorp.exam.service.impl.TransactionErrorimpl;
import com.ascendcorp.exam.service.impl.Unknownimpl;
import com.ascendcorp.exam.util.Response;
import com.ascendcorp.exam.util.ResponseBankType;

import lombok.NonNull;

@Service
public class InquiryService {

	private final BankProxyGateway bankProxyGateway;
	private final MessageConfig messageConfig;
	
	 
	@Autowired
	 public InquiryService(BankProxyGateway bankProxyGateway , MessageConfig messageConfig) {
		// TODO Auto-generated constructor stub
		 this.bankProxyGateway = bankProxyGateway;
		 this.messageConfig = messageConfig;
	}

	final static Logger log = Logger.getLogger(InquiryService.class);

	public InquiryServiceResultDTO inquiry(String transactionId, Date tranDateTime, String channel, String locationCode,
			String bankCode, String bankNumber, double amount, String reference1, String reference2, String firstName,
			String lastName) {

	
		InquiryServiceResultDTO respDTO = null;
		try {
			validateNull(transactionId, tranDateTime, channel, bankCode, bankNumber, amount);
			
			log.info("call bank web service");
            TransferResponse response = bankProxyGateway.requestTransfer(transactionId, tranDateTime, channel,
                    bankCode, bankNumber, amount, reference1, reference2);
            
            if(response == null)  throw new Exception("Unable to inquiry from service.");
            
            respDTO = new InquiryServiceResultDTO();
            respDTO.setRef_no1(response.getReferenceCode1());
            respDTO.setRef_no2(response.getReferenceCode2());
            respDTO.setAmount(response.getBalance());
            respDTO.setTranID(response.getBankTransactionID());
            
            HashMap<ResponseBankType ,ManageResponse> mapResponse = new HashMap<ResponseBankType, ManageResponse>();
            mapResponse.put(ResponseBankType.approved, new Approvedimpl());
            mapResponse.put(ResponseBankType.invalid_data, new InvalidDataimpl());
            mapResponse.put(ResponseBankType.transaction_error, new TransactionErrorimpl());
            mapResponse.put(ResponseBankType.unknown, new Unknownimpl());
            
            ResponseBankType key = ResponseBankType.valueOf(response.getResponseCode().toLowerCase());
            

            return mapResponse.get(key).process(response);
//        	throw new Exception("Unsupport Error Reason Code");		
		}catch (NullPointerException e) {
	                respDTO = new InquiryServiceResultDTO();
	                respDTO.setReasonCode(Response.NPE.getCode()+"");
	                respDTO.setReasonDesc(Response.NPE.getDesc());
		} catch(WebServiceException r) {
			String faultString = r.getMessage();
                respDTO = new InquiryServiceResultDTO();
                if(faultString != null && (faultString.indexOf("java.net.SocketTimeoutException") > -1
                        || faultString.indexOf("Connection timed out") > -1 )){
                    // bank timeout
                    respDTO.setReasonCode(Response.TIME_OUT.getCode()+"");
                    respDTO.setReasonDesc(Response.TIME_OUT.getDesc());
                }else{
                    // bank general error
                    respDTO.setReasonCode(Response.INTERNAL_APLLICATION_ERROR.getCode()+"");
                    respDTO.setReasonDesc(Response.INTERNAL_APLLICATION_ERROR.getDesc());
                }
		} catch (Exception e) {
			log.error("inquiry exception", e);
	            if(respDTO == null || (respDTO != null && respDTO.getReasonCode() == null)){
	                respDTO = new InquiryServiceResultDTO();
	                respDTO.setReasonCode(Response.INTERNAL_APLLICATION_ERROR.getCode()+"");
	                respDTO.setReasonDesc(Response.INTERNAL_APLLICATION_ERROR.getDesc());
	            }
		}
		

		return respDTO;

	}

	private void validateNull(@NonNull String transactionId, @NonNull Date tranDateTime,@NonNull String channel,
			@NonNull String bankCode,@NonNull String bankNumber, double amount) {
		if(StringUtils.isEmpty(bankCode)) throw new NullPointerException("Bank Code is required!");
		if(StringUtils.isEmpty(bankNumber)) throw new NullPointerException("Bank Number is required!");
		if(amount <= 0) throw new NullPointerException("Amount must more than zero!");

	}

}
